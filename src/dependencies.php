<?php
// DIC configuration

$container = $app->getContainer();

// Přidání konfigurace twigu.
$container['twig'] = function ($c) {
    // Viz nastavení v settings.php.
    $settings = $c->get('settings')['twig'];
    $view = new \Slim\Views\Twig($settings['templates_path'], $settings);

    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));

    return $view;
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};
