<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes

$app->get('/', function (Request $request, Response $response, array $args) {
    return $this->twig->render($response, 'index.html.twig');
});

$app->post('/objednat', function (Request $request, Response $response, array $args) {
    $form = $request->getParsedBody();

    // Tady by se uložila objednávka do databáze.

    // Sem se vyplní testovací údaje.
    $goId = 12345;
    $clientId = 12345;
    $clientSecret = "";

    // Vytvoření platby dle https://doc.gopay.com/cs/?lang=php#standardni-platba.
    $payment = [
        'payer' => [
            "default_payment_instrument" => "PAYMENT_CARD",
            "allowed_payment_instruments" => [
                \GoPay\Definition\Payment\PaymentInstrument::PAYMENT_CARD,
                \GoPay\Definition\Payment\PaymentInstrument::GOPAY,
                \GoPay\Definition\Payment\PaymentInstrument::PAYPAL,
            ],
            "contact" => [
                "first_name" => $form['firstName'],
                "last_name" => $form['firstName'],
                "email" => $form['email'],
                "city" => $form['city'],
                "street" => $form['address'],
                "postal_code" => $form['postalCode'],
                "country_code" => "CZE",
            ],
        ],
        "target" => [
            "type" => "ACCOUNT",
            "goid" => $goId,
        ],
        "amount" => 100,
        "currency" => "CZK",
        // Jen pro ukázkové účely -- generování náhodného čísla.
        "order_number" => random_int(1, 1000),
        "order_description" => "Testovací objednávka",
        "items" => [
            [
                "type" => \GoPay\Definition\Payment\PaymentItemType::ITEM,
                "name" => "Testovací položka",
                "amount" => 100,
                "count" => 1,
                "vat_rate" => 21
            ],
        ],
        "callback" => [
            // Po provedení objednávky se přesměruje na tuhle URL. Po zpracování response proběhne redirect na děkovnou stránku nebo oznámení o chybě při platbě.
            "return_url" => "http://localhost:8080/gopay/navrat",
            // Tohle se používá pro automatické notifikace z GoPay.
            "notification_url" => "http://localhost:8080/gopay/notifikace"
        ],
    ];

    $gopay = \GoPay\Api::payments([
        'goid' => $goId,
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
        'isProductionMode' => false,
    ]);

    $gopayResponse = $gopay->createPayment($payment);

    if ($gopayResponse->hasSucceed()) {
        /*
         * Tady by se napárovalo vrácené ID platby z GoPay na objednávku.
         */

        // Vrátí se response s přesměrováním na GoPay bránu.
        $gatewayUrl = $gopayResponse->json["gw_url"];
        return $response->withRedirect($gatewayUrl);
    }

    // Zapíše do logs/app.log chybu, která se vrátí z GoPay.
    $this->logger->error(json_encode($gopayResponse->json));
    return $this->twig->render($response, 'index.html.twig');
});
